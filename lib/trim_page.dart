import 'dart:io';
import 'dart:ui' as ui show Image;

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:video_trimmer/image_row.dart';

import 'generate_image.dart';

class TrimPage extends StatefulWidget {
  final File videoFile;
  TrimPage({@required this.videoFile});

  @override
  _TrimPageState createState() => _TrimPageState();
}

class _TrimPageState extends State<TrimPage> {
  VideoPlayerController _videoController;

  RangeValues _values = RangeValues(0.0, 1.0);

  // Image _imageInData;
  // ImageFormat _format = ImageFormat.JPEG;
  // int _quality = 50;
  // int _size = 0;
  // int _timeMs = 0;
  // int _imageDataSize;
  // ui.Image _uiImageInData;

  @override
  void initState() {
    super.initState();

    _videoController = VideoPlayerController.file(widget.videoFile)
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized,
        // even before the play button has been pressed.
        setState(() {});
      });
  }

  Future<int> getDuration() async {
    int duration;
    if (_videoController.value.initialized)
      duration = await _videoController.value.duration.inMilliseconds;

    return duration;
  }

  void refresh(RangeValues newValue) {
    // reload
    setState(() {
      _values = newValue;
    });
  }

  // Future<Image> _generateImage() async {
  //   if (_imageInData != null) {
  //     setState(() {
  //       _imageInData = null;
  //       _uiImageInData = null;
  //     });
  //   }

  //   final thumbnail = await VideoThumbnail.thumbnailData(
  //       video: widget.videoFile.path,
  //       imageFormat: _format,
  //       maxHeightOrWidth: _size,
  //       timeMs: getDuration()~/9 + getDuration()~/9 + getDuration()~/9,
  //       quality: _quality);

  //   _imageDataSize = thumbnail.length;
  //   print("image data size: $_imageDataSize");

  //   _imageInData = Image.memory(thumbnail)..image.resolve(ImageConfiguration());

  //   return _imageInData;
  // }

  @override
  Widget build(BuildContext context) {
    // getDuration();

    return FutureBuilder(
      future: getDuration(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return SafeArea(
            child: Scaffold(
              body: Container(
                color: Colors.black,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(height: 30),
                      Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              child: imageRow(
                                  widget.videoFile.path, snapshot.data),
                              decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.white, width: 2),
                              ),
                            ),
                          ),
                          Container(
                            height: 85,
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: RangeSlider(
                                max: 1,
                                values: _values,
                                activeColor: Colors.white,
                                onChanged: (newValue) {
                                  refresh(newValue);
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        } else {
          return Container(
            color: Colors.black,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );

    // return FutureBuilder(
    //   future: generateImage(
    //     widget.videoFile.path,
    //     12000,
    //   ),
    //   builder: (context, snapshot) {
    //     return SafeArea(
    //               child: Scaffold(
    //         body: Container(
    //           color: Colors.black,
    //           child: snapshot.hasData
    //               ? SizedBox(
    //                   width: 60,
    //                   child: snapshot.data,
    //                 )
    //               : SizedBox(),
    //         ),
    //       ),
    //     );
    //   },
    // );

    // return Scaffold(
    //   body: Container(
    //     color: Colors.black,
    //     // child: ListView(),
    //     child: Center(
    //       // Retrieve the video from storage and
    //       // Show the video in the center
    //       // child: _videoController.value.initialized
    //       //     ? AspectRatio(
    //       //         aspectRatio: _videoController.value.aspectRatio,
    //       //         child: VideoPlayer(_videoController),
    //       //       )
    //       //     : Container(
    //       //         child: Center(child: CircularProgressIndicator()),
    //       //       ),
    //       child: _generateImage().whenComplete(){
    //         return
    //       }
    //       // _imageInData != null
    //       //     ? _imageInData
    //       //     : SizedBox(
    //       //         child: Container(
    //       //           color: Colors.white,
    //       //         ),
    //       //         height: 200,
    //       //       ),
    //     ),
    //   ),
    //   // floatingActionButton: FloatingActionButton(
    //   //   onPressed: () {
    //   //     setState(() {
    //   //       _videoController.value.isPlaying
    //   //           ? _videoController.pause()
    //   //           : _videoController.play();
    //   //     });
    //   //   },
    //   //   child: Icon(
    //   //     _videoController.value.isPlaying ? Icons.pause : Icons.play_arrow,
    //   //   ),
    //   // ),
    // );
  }

  @override
  void dispose() {
    super.dispose();
    _videoController.dispose();
  }
}
