import 'package:flutter/material.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

Future<Image> generateImage(
  String videoFilePath,
  int timeInMilliseconds,
) async {
  Image _imageInData;
  ImageFormat _format = ImageFormat.JPEG;
  int _quality = 50;
  int _size = 0;
  int _imageDataSize;

  _imageInData = null;

  final thumbnail = await VideoThumbnail.thumbnailData(
      video: videoFilePath,
      imageFormat: _format,
      maxHeightOrWidth: _size,
      timeMs: timeInMilliseconds,
      quality: _quality);

  _imageDataSize = thumbnail.length;
  print("image data size: $_imageDataSize");

  _imageInData = Image.memory(thumbnail)..image.resolve(ImageConfiguration());

  return _imageInData;
}
