import 'package:flutter/material.dart';

import 'generate_image.dart';

// TODO: Use ratios for the width according to the mobile screen size
// TODO: Make the number of frames according to the width of the screen
// but keep the image width and height same, only
// increase or decrease the number of image frames shown
// on the screen at a point of time.

Widget imageRow(String videoFilePath, int duration) {
  int durationPart = duration ~/ 9;
  int initialDuration = 0;
  // int finalDuration = duration;
  double size = 40;

  List<Image> imageList = [];

  Future<List> formingImageList() async {
    for (int i = 0; i <= 8; i++) {
      await generateImage(videoFilePath, initialDuration + durationPart * i)
          .then((result) {
        imageList.add(result);
      });
    }

    // await generateImage(videoFilePath, finalDuration).then((result) {
    //   imageList.add(result);
    // });

    return imageList;
  }

  return FutureBuilder(
    future: formingImageList(),
    builder: (context, snapshot) {
      if (snapshot.hasData) {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              width: size,
              height: size,
              child: Image(
                image: imageList.elementAt(0).image,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: size,
              height: size,
              child: Image(
                image: imageList.elementAt(1).image,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: size,
              height: size,
              child: Image(
                image: imageList.elementAt(2).image,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: size,
              height: size,
              child: Image(
                image: imageList.elementAt(3).image,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: size,
              height: size,
              child: Image(
                image: imageList.elementAt(4).image,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: size,
              height: size,
              child: Image(
                image: imageList.elementAt(5).image,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: size,
              height: size,
              child: Image(
                image: imageList.elementAt(6).image,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: size,
              height: size,
              child: Image(
                image: imageList.elementAt(7).image,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: size,
              height: size,
              child: Image(
                image: imageList.elementAt(8).image,
                fit: BoxFit.cover,
              ),
            ),
            // SizedBox(
            //   width: width,
            //   child: imageList.elementAt(6),
            // ),
            // ),
            // SizedBox(
            //   width: width,
            //   child: imageList.elementAt(7),
            // ),
            // SizedBox(
            //   width: width,
            //   child: imageList.elementAt(8),
            // ),
          ],
        );
      } else {
        return SizedBox();
      }
    },
  );
}
