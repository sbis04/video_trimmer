import 'dart:io';
import 'dart:ui' as ui show Image;

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import 'trim_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  File video;

  Image _imageInData;
  ImageFormat _format = ImageFormat.JPEG;
  int _quality = 50;
  int _size = 0;
  int _timeMs = 0;
  int _imageDataSize;
  ui.Image _uiImageInData;

  void _generateImage() async {
    if (_imageInData != null) {
      setState(() {
        _imageInData = null;
        _uiImageInData = null;
      });
    }

    final thumbnail = await VideoThumbnail.thumbnailData(
        video: video.path,
        imageFormat: _format,
        maxHeightOrWidth: _size,
        timeMs: _timeMs,
        quality: _quality);

    _imageDataSize = thumbnail.length;
    print("image data size: $_imageDataSize");

    _imageInData = Image.memory(thumbnail)
      ..image
          .resolve(ImageConfiguration())
          .addListener(ImageStreamListener((ImageInfo info, bool _) {
        setState(() {
          _uiImageInData = info.image;
        });
      }));
  }

  // To get the video file from the local storage
  Future<File> _openFileExplorer() async {
    video = await ImagePicker.pickVideo(source: ImageSource.gallery);
    return video;
  }

  // This method is called for passing the video file
  // to the next screen for further operarion
  // called when the "Choose Video" button is tapped
  // @param: file
  void _onLoading() {
    _openFileExplorer().then((file) {
      if (file != null) {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => TrimPage(videoFile: file),
          ),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Video Trimer'),
        backgroundColor: Colors.orange[800],
      ),
      body: Container(
        color: Colors.orange,
        child: Center(
          child: RaisedButton(
            color: Colors.orange[800],
            textColor: Colors.black,
            onPressed: () {
              _onLoading();
            },
            // onPressed: _onLoading(),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            child: Text('Choose video'),
          ),
        ),
      ),
    );
  }
}
